<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStyleRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('style_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',191);
            $table->integer('price');
            $table->integer('promotion_price');
            $table->text('image')->nullable();
            $table->text('content');
            $table->string('amount',191);
            $table->string('size',191);
            $table->text('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('style_rooms');
    }
}
