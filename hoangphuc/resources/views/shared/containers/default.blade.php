<div id="page">
    @include('shared.headers.default')
    @yield('content')
    @include('shared.footers.default')
</div>
