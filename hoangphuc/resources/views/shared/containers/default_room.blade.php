<div id="page" class="theia-exception">
    @include('shared.headers.headers_room')
    @yield('content')
    @include('shared.footers.default')
</div>