@extends('layouts.default')
@push('head')
<meta name="keywords" content="motel, nhà nghỉ, nhà nghỉ bình dân, nhà nghỉ ở huế">
<meta name="author" content="stayhuehotel">
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta property="og:locale" content="vi_VN">
<meta property="og:type" content="article">
<meta property="og:title" content="{{ $information->name }}">
<meta property="og:url" content="">
<link rel="canonical" href="">
<meta property="og:image" content="{{ Voyager::image( method_exists($information, 'thumbnail') ? $information->thumbnail('cropped') : $information->image ) }}">
<meta property="og:image:alt" content="{{ $information->name }}">
<meta property="og:image:width" content="819">
<meta property="og:image:height" content="1024">
<meta property="og:description" content="Hoàng Phúc Motel có vị trí tọa lạc ở 63 Nguyễn Sinh Cung, P. Vỹ Dạ, Thừa Thiên Huế. Với các dịch vụ phòng vô cùng tiện nghi, đem đến cho du khách một chất lượng tốt nhất để quý du khách có sự hài lòng với dịch vụ nghỉ dưỡng chúng tôi. Hơn hết đó chính là giá cả, Hoàng Phúc Motel có giá tương đối rẻ và phù hợp với rất nhiều du khách khi đến đây...">
@endpush
@section('title')
{{$information->name}}
@endsection
@section('content')
    <main>
        @include('sections.home.hero_single')
        @include('sections.home.about')
        @include('sections.home.container-fluid')
        @include('sections.home.news_event')
        @include('sections.home.media-gallery')
    </main>
@endsection
