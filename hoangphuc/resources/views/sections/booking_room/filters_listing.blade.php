<div class="filters_listing version_2 ">
    <div class="container">
        <ul class="clearfix">
            <li>
                <a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap" data-text-swap="Ẩn bản đồ" data-text-original="Xem bản đồ">Xem bản đồ</a>
            </li>
        </ul>
    </div>
    <!-- /container -->
</div>
<div class="collapse" id="collapseMap">
    <div id="map" class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3825.9784114036656!2d107.59325981413721!3d16.47663078863038!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a121988773f9%3A0xb97bfd487ba06161!2zNjMgTmd1eeG7hW4gU2luaCBDdW5nLCBW4bu5IEThuqEsIFRow6BuaCBwaOG7kSBIdeG6vywgVGjhu6thIFRoacOqbiBIdeG6vywgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1573873055847!5m2!1svi!2s" width="100%" height="530" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
</div>
