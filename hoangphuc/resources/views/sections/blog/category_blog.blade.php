
        <div class="widget">
            <div class="widget-title">
                <h4>THỂ LOẠI</h4>
            </div>
            <ul class="cats">
                @foreach($categories as $category)
                <li><a  href="{{ route('blog.category',$category->slug) }}">{{ $category->name }}
                    ({{ count($category->posts) }})
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
   
 