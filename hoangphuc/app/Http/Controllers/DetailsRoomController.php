<?php

namespace App\Http\Controllers;

use App\Information;
use App\Service;
use App\ServiceOther;
use App\StyleRoom;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Category;

class DetailsRoomController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($slug,Request $request,ServiceOther $serviceOtherModel ,Category $categoryModel,Information $informationModel,StyleRoom $styleRoomModel,Service $serviceModel)
    {
        $information = $informationModel->get()->first();
        $styleRooms =$styleRoomModel->get();
        $categories = $categoryModel->get();
        $services = $serviceModel->get();
        $servicess =$serviceOtherModel->get();
        $styleRoom = $styleRoomModel->where('slug',$slug)->firstOrFail();
        $viewData = compact('information','styleRoom','styleRooms','categories','servicess','services');
        return view('pages.detail_room',$viewData);
    }
}
